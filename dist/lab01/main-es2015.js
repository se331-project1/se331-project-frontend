(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-my-nav></app-my-nav>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/login.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-grid-list cols=\"1\" rowHeight=\"fit\">\n  <form class=\"form-login\" [formGroup]=\"addressForm\" novalidate (ngSubmit)=\"onSubmit()\">\n    <mat-card class=\"container\">\n      <mat-card-title class=\"login-text\">\n        <img  class=\"logo\" src=\"../../../assets/images/camt.jpg\" />\n      </mat-card-title>\n      <mat-card-subtitle *ngIf=\"!home\">\n        Please login to access the resource\n      </mat-card-subtitle>\n      <mat-card-content>\n        <div class=\"row\">\n          <div class=\"col-4\">\n            <mat-form-field class=\"full-width\">\n              <input matInput placeholder=\"User name\" formControlName=\"username\">\n              <mat-icon matSuffix (click)=\"hide = !hide\">{{'person'}}</mat-icon>\n              <mat-error *ngFor=\"let validation of validation_messages.username\">\n                <mat-error class=\"error-message\" *ngIf=\"addressForm.get('username').hasError(validation.type)\" [innerHTML]=validation.message>\n                </mat-error>\n              </mat-error>\n            </mat-form-field>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-4\">\n            <mat-form-field class=\"full-width\">\n              <input matInput placeholder=\"Enter your password\" [type]=\"hide ? 'password' : 'text'\" formControlName=\"password\">\n              <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\n              <mat-error *ngFor=\"let validation of validation_messages.password\">\n                <mat-error class=\"error-message\" *ngIf=\"addressForm.get('password').hasError(validation.type)\" [innerHTML]=validation.message>\n                </mat-error>\n              </mat-error>\n            </mat-form-field>\n          </div>\n        </div>\n      </mat-card-content>\n      <mat-card-actions>\n        <mat-error *ngIf=\"isError\">\n            <strong>The username, and password is incorrect</strong>\n        </mat-error>\n        <div class=\"row submit-button-style\">\n          <button class=\"login-button\" mat-raised-button color=\"primary\" type=\"submit\" [disabled]=\"!addressForm.valid\">Log in</button>        \n        </div>\n      </mat-card-actions>\n    </mat-card>\n  </form>\n</mat-grid-list>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/my-nav/my-nav.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/my-nav/my-nav.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"sidenav-container\">\n    <mat-sidenav-content>\n        <mat-toolbar class=\"effect-3\"  *ngIf=\"hasRole('LECTURER,STUDENT')\">\n            <span><img class=\"logo\" src=\"../../assets/images/camt-icon.png\" /></span>\n            <button class=\"menu-button\" type=\"button\" mat-icon-button>\n                <a mat-list-item [routerLink]=\"['/list']\" routerLinkActive=\"router-link-active\">List</a>\n            </button>\n            <button class=\"menu-button\" type=\"button\" mat-icon-button>\n                <a mat-list-item [routerLink]=\"['/view']\" routerLinkActive=\"router-link-active\">View</a>\n            </button>\n            <button class=\"menu-button\" type=\"button\" mat-icon-button>\n                <a mat-list-item [routerLink]=\"['/login']\" routerLinkActive=\"router-link-active\">Login</a>\n            </button>\n            <button class=\"menu-button\" type=\"button\" mat-icon-button>\n                <a *ngIf=\"hasRole('LECTURER')\" mat-list-item [routerLink]=\"['/add']\" routerLinkActive=\"router-link-active\">Add</a>\n            </button>\n            <div class=\"profile-button\">\n                <button type=\"button\" mat-icon-button (click)=\"openDialog()\">\n                    <a mat-list-item routerLinkActive=\"router-link-active\"><mat-icon class=\"log-icon\" matSuffix (click)=\"hide = !hide\">{{'account_circle'}}</mat-icon></a>\n                </button>\n            </div>\n        </mat-toolbar>\n        <!-- Add Content Here -->\n        <router-outlet></router-outlet>\n    </mat-sidenav-content>\n</mat-sidenav-container>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"user\">\n    <img mat-card-lg-image [src]=\"user.image || defaultImageUrl\">\n   <div class=\"user-name\">\n    <mat-card-title>\n        Name: {{user.name}}\n       </mat-card-title>\n   </div>\n   <div class=\"user-name\">\n    <mat-card-title>\n        Role: {{getUserRole()}}\n       </mat-card-title>\n   </div>\n   <div class=\"button-logout\">\n        <mat-card-title>\n            <button type=\"button\" (click)=\"userLogout()\">Log out</button>\n       </mat-card-title>\n   </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/file-not-found/file-not-found.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/file-not-found/file-not-found.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<H2>The resource you have asked is not in the server</H2>\n<img src=\"assets/images/file-not-found.jpg\">"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/add/students.add.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/add/students.add.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar>\n    <mat-toolbar-row>\n        <span>Add new student</span>\n    </mat-toolbar-row>\n</mat-toolbar>\n<div>\n</div>\n<form style=\"margin-left: 1em\" [formGroup]=\"form\" (submit)=\"submit()\">\n    <mat-form-field class=\"full-width\">\n        <input matInput placeholder=\"Student Id\" formControlName=\"studentId\" required #spy>\n        <mat-error *ngFor=\"let validation of validation_messages.studentId\">\n            <mat-error class=\"error-message\" *ngIf=\"form.get('studentId').hasError(validation.type)\n                    && (form.get('studentId').dirty || form.get('studentId').touched)\">\n                {{validation.message}}\n            </mat-error>\n        </mat-error>\n    </mat-form-field>\n    <table class=\"full-width\" cellspacing=\"0\">\n        <tr>\n            <td>\n                <mat-form-field class=\"full-width\">\n                    <input matInput placeholder=\"First name\" formControlName=\"name\" required>\n                    <mat-error *ngFor=\"let validation of validation_messages.studentId\">\n                        <mat-error class=\"error-message\" *ngIf=\"form.get('name').hasError(validation.type)\n                                && (form.get('name').dirty || form.get('name').touched)\">\n                            {{validation.message}}\n                        </mat-error>\n                    </mat-error>\n                </mat-form-field>\n            </td>\n            <td>\n                <mat-form-field class=\"full-width\">\n                    <input matInput placeholder=\"surname\" formControlName=\"surname\" required>\n                    <mat-error *ngFor=\"let validation of validation_messages.surname\">\n                        <mat-error class=\"error-message\" *ngIf=\"form.get('surname').hasError(validation.type)\n                                && (form.get('surname').dirty || form.get('surname').touched)\">\n                            {{validation.message}}\n                        </mat-error>\n                    </mat-error>\n                </mat-form-field>\n            </td>\n        </tr>\n        <div>\n            <button mat-raised-button type=\"button\" class=\"center-button\" (click)=\"downQuantity(student)\">-</button>\n            <mat-form-field class=\"penInput\">\n                <input matInput placeholder=\"pen Amount\" class=\"center-text\" value=\"0\" formControlName=\"penAmount\">\n                <mat-error *ngFor=\"let validation of validation_messages.penAmount\">\n                    <mat-error class=\"error-message\" *ngIf=\"form.get('penAmount').hasError(validation.type)\n                            && (form.get('penAmount').dirty || form.get('penAmount').touched)\">\n                        {{validation.message}}\n                    </mat-error>\n                </mat-error>\n            </mat-form-field>\n            <button mat-raised-button layout=\"row\" type=\"button\" class=\"center-button\" (click)=\"upQuantity(student)\">+</button>\n        </div>\n    </table>\n    <!-- <mat-form-field hintLabel=\"will change to the image uploader component later\" class=\"full-width\">\n        <input matInput placeholder=\"file name\" formControlName=\"image\">\n        <mat-error *ngFor=\"let validation of validation_messages.image\">\n            <mat-error class=\"error-message\" *ngIf=\"form.get('image').hasError(validation.type)\n                    && (form.get('image').dirty || form.get('image').touched)\">\n                {{validation.message}}\n            </mat-error>\n        </mat-error>\n    </mat-form-field> -->\n    <form>\n        <mat-file-upload [labelText]=\"'Select your images:'\" [selectButtonText]=\"'Choose File'\" [uploadButtonText]=\"'Submit'\" [allowMultipleFiles]=\"false\" [showUploadButton]=\"true\" [acceptedTypes]=\"'.png, .jpg, .jpeg'\" [customSvgIcon]=\"'close_custom'\" (uploadClicked)=\"onUploadClicked($event)\"\n            (selectedFilesChanged)=\"onSelectedFilesChanged($event)\"></mat-file-upload>\n        <mat-progress-bar mode=\"determinate\" [value]=\"progress\" *ngIf=\"progress > 0\"></mat-progress-bar>\n    </form>\n    <mat-form-field class=\"full-width\">\n        <textarea matInput placeholder=\"Description\" formControlName=\"description\"></textarea>\n        <mat-error *ngFor=\"let validation of validation_messages.description\">\n            <mat-error class=\"error-message\" *ngIf=\"form.get('description').hasError(validation.type)\n                    && (form.get('description').dirty || form.get('description').touched)\">\n                {{validation.message}}\n            </mat-error>\n        </mat-error>\n    </mat-form-field>\n    <div class=\"center\">\n        <button mat-raised-button color=\"primary\" type=\"submit\" [disabled]=\"!form.valid\">Add</button>\n    </div>\n</form>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/list/students.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/list/students.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row justify-content-start\">\n  <div class=\"col-md-offset-3\t col-md-9\">\n    <div *ngFor=\"let student of students\">\n      <div class=\"panel panel-primary  \">\n        <div class=\"panel-heading\">\n          <h2 class=\"panel-title\">{{student.studentId }}</h2>\n        </div>\n        <div class=\"panel-body row\">\n          <div class=\"col-md-4\">\n            <p class=\"student-name\"> {{student. name | uppercase}} {{student.surname}}</p>\n            <p *ngIf=\"student.gpa > 2.5\">Good Student who get grade {{student.gpa | number:'1.2-2'}}</p>\n            <p *ngIf=\"student.gpa <= 2.5\">Bad Student who get grade {{student.gpa | number:'1.2-2'}}</p>\n          </div>\n          <div class=\"col-md-4\">\n            <img [src]=\"student.image\" [title]=\"student.name\">\n          </div>\n        </div>\n        <div class=\"row align-items-center\">\n          <button type=\"button\" class=\"btn btn-primary btn-xs col-md-1\" (click)=\"downQuantity(student)\">-</button>\n          <input type=\"text\" class=\"col-md-1\" [(ngModel)]=\"student.penAmount\">\n          <button type=\"button\" class=\"btn btn-primary btn-xs col-md-1\" (click)=\"upQuantity(student)\">+</button>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"row justify-content-end grade\">\n  <div class=\"alert alert-success col-md-offset-2 col-md-6\"> The average gpa is {{averageGpa()}} </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/student-table/student-table.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/student-table/student-table.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n\n    <div *ngIf=\"loading\" class=\"loading-indicator\">\n        <mat-spinner></mat-spinner>\n    </div>\n\n    <mat-form-field>\n        <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n    </mat-form-field>\n    <table mat-table class=\"full-width-table\" matSort aria-label=\"Elements\">\n        <!-- Id Column -->\n        <ng-container matColumnDef=\"id\">\n            <th mat-header-cell *matHeaderCellDef>Id</th>\n            <td mat-cell *matCellDef=\"let student\">{{student.id}}</td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n        <!-- StudentId Column -->\n        <ng-container matColumnDef=\"studentId\">\n            <th mat-header-cell *matHeaderCellDef>Student id</th>\n            <td mat-cell *matCellDef=\"let student\">{{student.studentId}}</td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n        <!-- Name Column -->\n        <ng-container matColumnDef=\"name\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Name</th>\n            <td mat-cell *matCellDef=\"let student\">{{student.name}}</td>\n            <td mat-footer-cell *matFooterCellDef>Total </td>\n        </ng-container>\n        <!-- Surname Column -->\n        <ng-container matColumnDef=\"surname\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Surname</th>\n            <td mat-cell *matCellDef=\"let student\">{{student.surname}}</td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n        <!-- pen amount Column -->\n        <ng-container matColumnDef=\"penAmount\">\n            <th mat-header-cell *matHeaderCellDef class=\"center-text\">Pen</th>\n            <td mat-cell *matCellDef=\"let student\">\n                <div class=\"center\">\n                    <button mat-raised-button class=\"center-button\" (click)=\"downQuantity(student)\">-</button>\n                    <mat-form-field class=\"penInput\">\n                        <input matInput class=\"center-text penInput\" [(ngModel)]=\"student.penAmount\">\n                    </mat-form-field>\n                    <button mat-raised-button layout=\"row\" class=\"center-button\" (click)=\"upQuantity(student)\">+</button>\n                </div>\n            </td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n        <!-- Gpa Column -->\n        <ng-container matColumnDef=\"gpa\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>GPA</th>\n            <td mat-cell *matCellDef=\"let student\">{{student.gpa}}</td>\n            <td mat-footer-cell *matFooterCellDef> {{averageGpa() | number:'1.2-2'}} </td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n        <!-- image Column -->\n        <ng-container matColumnDef=\"image\">\n            <th mat-header-cell *matHeaderCellDef class=\"center-text\">Image</th>\n            <td mat-cell *matCellDef=\"let student\" class=\"center-text\">\n                <img [src]=\"student.image\" [title]=\"student.name\" class=\"img-fluid\">\n            </td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n        <tr mat-footer-row *matFooterRowDef=\"displayedColumns\"></tr>\n    </table>\n\n    <mat-paginator #paginator [length]=\"dataSource?.data.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n    </mat-paginator>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/view/students.view.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/view/students.view.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar>\n    <mat-toolbar-row>\n        <span>Student</span>\n    </mat-toolbar-row>\n</mat-toolbar>\n<form style=\"margin-left: 1em\">\n    <mat-form-field class=\"full-width\">\n        <input matInput placeholder=\"Student Id\" [value]=\"student?.studentId\" readonly>\n    </mat-form-field>\n    <table class=\"full-width\" cellspacing=\"0\">\n        <tr>\n            <td>\n                <mat-form-field class=\"full-width\">\n                    <input matInput placeholder=\"First name\" [value]=\"student?.name\" readonly>\n                </mat-form-field>\n            </td>\n            <td>\n                <mat-form-field class=\"full-width\">\n                    <input matInput placeholder=\"surname\" [value]=\"student?.surname\" readonly>\n                </mat-form-field>\n            </td>\n        </tr>\n        <mat-form-field class=\"penInput\">\n            <input matInput placeholder=\"pen Amount\" class=\"center-text\" [value]=\"student?.penAmount\" readonly>\n        </mat-form-field>\n    </table>\n    <div class=\"center\">\n        <mat-card class=\"img-width\">\n            <mat-card-header>\n                <mat-card-title>Image</mat-card-title>\n            </mat-card-header>\n            <img mat-card-image [attr.src]=\"student?.image\" [alt]=\"student?.name\">\n        </mat-card>\n    </div>\n    <mat-form-field class=\"full-width\">\n        <textarea matInput placeholder=\"Description\" [value]=\"student?.description\" readonly></textarea>\n    </mat-form-field>\n</form>"

/***/ }),

/***/ "./src/app/Guard/lecturer-and-student-guard.ts":
/*!*****************************************************!*\
  !*** ./src/app/Guard/lecturer-and-student-guard.ts ***!
  \*****************************************************/
/*! exports provided: StudentAndLecturerGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentAndLecturerGuard", function() { return StudentAndLecturerGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _authentication_authentication_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../authentication/authentication-service.service */ "./src/app/authentication/authentication-service.service.ts");




let StudentAndLecturerGuard = class StudentAndLecturerGuard {
    constructor(router, authService) {
        this.router = router;
        this.authService = authService;
    }
    canActivate(next, state) {
        if (this.authService.hasRole('STUDENT') ||
            this.authService.hasRole('LECTURER')) {
            return true;
        }
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }
        });
        return false;
    }
};
StudentAndLecturerGuard.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _authentication_authentication_service_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationServiceService"] }
];
StudentAndLecturerGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], StudentAndLecturerGuard);



/***/ }),

/***/ "./src/app/Guard/lecturer-guard.ts":
/*!*****************************************!*\
  !*** ./src/app/Guard/lecturer-guard.ts ***!
  \*****************************************/
/*! exports provided: LecturerGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LecturerGuard", function() { return LecturerGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _authentication_authentication_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../authentication/authentication-service.service */ "./src/app/authentication/authentication-service.service.ts");




let LecturerGuard = class LecturerGuard {
    constructor(router, authService) {
        this.router = router;
        this.authService = authService;
    }
    canActivate(next, state) {
        if (this.authService.hasRole('LECTURER')) {
            return true;
        }
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }
        });
        return false;
    }
};
LecturerGuard.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _authentication_authentication_service_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationServiceService"] }
];
LecturerGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LecturerGuard);



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/file-not-found/file-not-found.component */ "./src/app/shared/file-not-found/file-not-found.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");





const appRoutes = [
    {
        path: '',
        redirectTo: '/list',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"]
    },
    { path: '**', component: _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_3__["FileNotFoundComponent"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(appRoutes)
        ],
        exports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
        ]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.name = 'SE331';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./shared/file-not-found/file-not-found.component */ "./src/app/shared/file-not-found/file-not-found.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm2015/layout.js");
/* harmony import */ var mat_file_upload__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! mat-file-upload */ "./node_modules/mat-file-upload/fesm2015/mat-file-upload.js");
/* harmony import */ var _my_nav_my_nav_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./my-nav/my-nav.component */ "./src/app/my-nav/my-nav.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _service_student_rest_impl_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./service/student-rest-impl.service */ "./src/app/service/student-rest-impl.service.ts");
/* harmony import */ var _students_student_routing_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./students/student-routing.module */ "./src/app/students/student-routing.module.ts");
/* harmony import */ var _service_student_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./service/student-service */ "./src/app/service/student-service.ts");
/* harmony import */ var _students_student_table_student_table_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./students/student-table/student-table.component */ "./src/app/students/student-table/student-table.component.ts");
/* harmony import */ var _students_add_students_add_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./students/add/students.add.component */ "./src/app/students/add/students.add.component.ts");
/* harmony import */ var _students_list_students_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./students/list/students.component */ "./src/app/students/list/students.component.ts");
/* harmony import */ var _students_view_students_view_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./students/view/students.view.component */ "./src/app/students/view/students.view.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _helpers_jwt_iterceptor_service_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./helpers/jwt-iterceptor-service.service */ "./src/app/helpers/jwt-iterceptor-service.service.ts");
/* harmony import */ var _my_nav_user_profile_dialogue_user_profile_dialogue_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./my-nav/user-profile-dialogue/user-profile-dialogue.component */ "./src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component.ts");

























let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_13__["NgModule"])({
        entryComponents: [_my_nav_my_nav_component__WEBPACK_IMPORTED_MODULE_12__["MyNavComponent"], _my_nav_user_profile_dialogue_user_profile_dialogue_component__WEBPACK_IMPORTED_MODULE_23__["UserProfileDialogueComponent"]],
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _students_list_students_component__WEBPACK_IMPORTED_MODULE_19__["StudentsComponent"],
            _students_add_students_add_component__WEBPACK_IMPORTED_MODULE_18__["StudentsAddComponent"],
            _students_view_students_view_component__WEBPACK_IMPORTED_MODULE_20__["StudentsViewComponent"],
            _my_nav_my_nav_component__WEBPACK_IMPORTED_MODULE_12__["MyNavComponent"],
            _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_8__["FileNotFoundComponent"],
            _students_student_table_student_table_component__WEBPACK_IMPORTED_MODULE_17__["StudentTableComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_21__["LoginComponent"],
            _my_nav_user_profile_dialogue_user_profile_dialogue_component__WEBPACK_IMPORTED_MODULE_23__["UserProfileDialogueComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_10__["LayoutModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
            _students_student_routing_module__WEBPACK_IMPORTED_MODULE_15__["StudentRoutingModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
            mat_file_upload__WEBPACK_IMPORTED_MODULE_11__["MatFileUploadModule"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialogModule"]
        ],
        providers: [
            { provide: _service_student_service__WEBPACK_IMPORTED_MODULE_16__["StudentService"], useClass: _service_student_rest_impl_service__WEBPACK_IMPORTED_MODULE_14__["StudentRestImplService"] },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HTTP_INTERCEPTORS"], useClass: _helpers_jwt_iterceptor_service_service__WEBPACK_IMPORTED_MODULE_22__["JwtIterceptorServiceService"], multi: true }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/authentication/authentication-service.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/authentication/authentication-service.service.ts ***!
  \******************************************************************/
/*! exports provided: AuthenticationServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationServiceService", function() { return AuthenticationServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let AuthenticationServiceService = class AuthenticationServiceService {
    constructor(http) {
        this.http = http;
    }
    login(username, password) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].authenticationApi, { username: username, password: password
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(data => {
            if (data.token && data.user) {
                localStorage.setItem('token', data.token);
                localStorage.setItem('currentUser', JSON.stringify(data.user));
            }
        }));
    }
    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('currentUser');
    }
    getToken() {
        const token = localStorage.getItem('token');
        return token ? token : '';
    }
    getCurrentUser() {
        const currentUser = localStorage.getItem('currentUser');
        if (currentUser) {
            return JSON.parse(currentUser);
        }
        else {
            return null;
        }
    }
    hasRole(role) {
        const user = this.getCurrentUser();
        if (user) {
            const roleList = role.split(',');
            for (let j = 0; j < roleList.length; j++) {
                const authList = user.authorities;
                const userRole = 'ROLE_' + roleList[j].trim().toUpperCase();
                for (let i = 0; i < authList.length; i++) {
                    if (authList[i].name === userRole) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
};
AuthenticationServiceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
AuthenticationServiceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthenticationServiceService);



/***/ }),

/***/ "./src/app/helpers/jwt-iterceptor-service.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/helpers/jwt-iterceptor-service.service.ts ***!
  \***********************************************************/
/*! exports provided: JwtIterceptorServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtIterceptorServiceService", function() { return JwtIterceptorServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let JwtIterceptorServiceService = class JwtIterceptorServiceService {
    constructor() { }
    intercept(request, next) {
        const token = localStorage.getItem('token');
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                }
            });
        }
        return next.handle(request);
    }
};
JwtIterceptorServiceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], JwtIterceptorServiceService);



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    max-width: 380px;\n  }\n  .form-login {\n    opacity: 0.95;\n    padding-left: 60px;\n    padding-right: 60px;\n    padding-top: 160px;\n    padding-bottom: 240px;\n    border-radius: 10px;\n    border-width: 5px;\n  }\n  .logo{\n    width: 80px;\n    align-self: center;\n    padding: 10px;\n  }\n  mat-grid-list {\n    background-image: url('camt-bg.jpg');\n    width: 100%;\n    height: 100%;\n    background-size: cover;\n  }\n  mat-card-content {\n      align-self: center;\n  }\n  mat-card-actions {\n    align-self: center;\n  }\n  mat-card-subtitle {\n    align-self: center;\n  }\n  .submit-button-style {\n      margin-bottom: 30px;\n  }\n  .login-text {\n      align-self: center;\n      padding: 5px;\n  }\n  mat-form-field{\n    color :  #BB5544;\n  }\n  mat-form-field input{\n    color :  #000;\n  }\n  .login-button {\n    margin-top: 20px;\n    background-color: #BB5544;\n    \n  }\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLGdCQUFnQjtFQUNsQjtFQUNBO0lBQ0UsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsaUJBQWlCO0VBQ25CO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLGFBQWE7RUFDZjtFQUNBO0lBQ0Usb0NBQXdEO0lBQ3hELFdBQVc7SUFDWCxZQUFZO0lBQ1osc0JBQXNCO0VBQ3hCO0VBQ0E7TUFDSSxrQkFBa0I7RUFDdEI7RUFDQTtJQUNFLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0Usa0JBQWtCO0VBQ3BCO0VBQ0E7TUFDSSxtQkFBbUI7RUFDdkI7RUFDQTtNQUNJLGtCQUFrQjtNQUNsQixZQUFZO0VBQ2hCO0VBQ0E7SUFDRSxnQkFBZ0I7RUFDbEI7RUFDQTtJQUNFLGFBQWE7RUFDZjtFQUNBO0lBQ0UsZ0JBQWdCO0lBQ2hCLHlCQUF5Qjs7RUFFM0IiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIG1heC13aWR0aDogMzgwcHg7XG4gIH1cbiAgLmZvcm0tbG9naW4ge1xuICAgIG9wYWNpdHk6IDAuOTU7XG4gICAgcGFkZGluZy1sZWZ0OiA2MHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDYwcHg7XG4gICAgcGFkZGluZy10b3A6IDE2MHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAyNDBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGJvcmRlci13aWR0aDogNXB4O1xuICB9XG4gIC5sb2dve1xuICAgIHdpZHRoOiA4MHB4O1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICB9XG4gIG1hdC1ncmlkLWxpc3Qge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vYXNzZXRzL2ltYWdlcy9jYW10LWJnLmpwZycpO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICB9XG4gIG1hdC1jYXJkLWNvbnRlbnQge1xuICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICB9XG4gIG1hdC1jYXJkLWFjdGlvbnMge1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgfVxuICBtYXQtY2FyZC1zdWJ0aXRsZSB7XG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICB9XG4gIC5zdWJtaXQtYnV0dG9uLXN0eWxlIHtcbiAgICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG4gIH1cbiAgLmxvZ2luLXRleHQge1xuICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgcGFkZGluZzogNXB4O1xuICB9XG4gIG1hdC1mb3JtLWZpZWxke1xuICAgIGNvbG9yIDogICNCQjU1NDQ7XG4gIH1cbiAgbWF0LWZvcm0tZmllbGQgaW5wdXR7XG4gICAgY29sb3IgOiAgIzAwMDtcbiAgfVxuICAubG9naW4tYnV0dG9uIHtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNCQjU1NDQ7XG4gICAgXG4gIH1cblxuIl19 */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _authentication_authentication_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../authentication/authentication-service.service */ "./src/app/authentication/authentication-service.service.ts");
/* harmony import */ var _service_data_sharing_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/data-sharing.service */ "./src/app/service/data-sharing.service.ts");






let LoginComponent = class LoginComponent {
    constructor(fb, router, authenService, route, dataSharingService) {
        this.fb = fb;
        this.router = router;
        this.authenService = authenService;
        this.route = route;
        this.dataSharingService = dataSharingService;
        this.addressForm = this.fb.group({
            username: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.validation_messages = {
            'username': [
                { type: 'required', message: 'username is <strong>required</strong>' },
            ],
            'password': [
                { type: 'required', message: 'the password is <strong>required</strong>' }
            ]
        };
        this.hide = true;
    }
    ngOnInit() {
        this.isError = false;
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        if (this.returnUrl === '/') {
            this.home = true;
        }
        else {
            this.home = false;
        }
        console.log('returnUrl' + this.returnUrl);
    }
    onSubmit() {
        const value = this.addressForm.value;
        this.authenService.login(value.username, value.password)
            .subscribe(data => {
            this.isError = false;
            this.router.navigate([this.returnUrl]);
        }, error => {
            this.isError = true;
        });
        this.dataSharingService.isUserLoggedIn.next(true);
        console.log(this.addressForm.value);
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _authentication_authentication_service_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationServiceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _service_data_sharing_service__WEBPACK_IMPORTED_MODULE_5__["DataSharingService"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/my-nav/my-nav.component.css":
/*!*********************************************!*\
  !*** ./src/app/my-nav/my-nav.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n  height: 100%;\n}\n\n.sidenav {\n  width: 200px;\n}\n\n.sidenav .mat-toolbar {\n  background: inherit;\n}\n\n.mat-toolbar.mat-primary {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0;\n  z-index: 1;\n}\n\nimg {\n  border-radius: 50%;\n}\n\nbutton a {\n  text-decoration: none;\n  color: aliceblue;\n  font-weight: bolder;\n  font-family: Georgia, 'Times New Roman', Times, serif;\n}\n\nmat-toolbar {\n  background-color: #BB5544;\n}\n\nspan img {\n  width: 50px;\n  margin-right: 20px;\n}\n\n.menu-button {\n  margin-left: 5px;\n  margin-right: 5px;\n  padding-left: 35px;\n  padding-right: 35px;\n}\n\n/* HOVER EFFECT 3 */\n\n.effect-3 a:before,\n.effect-3 a:after {\n\tcontent: \"\";\n\theight: 1px;\n\twidth: 0;\n\topacity: 0;\n\tbackground-color: #fff;\n\tposition: absolute;\n\ttransition: all .5s;\n}\n\n.effect-3 a:before {\n\ttop: -3px;\n}\n\n.effect-3 a:after {\n\tbottom: -3px;\n\tright: 0;\n}\n\n.effect-3 a:hover:before,\n.effect-3 a:hover:after {\n\twidth: calc(100% + 20px);\n\topacity: 0.7;\n}\n\n.profile-button {\n  width: 50px;\n  right: 10px;\n  position: absolute;\n}\n\n.log-icon {\n  padding-top: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXktbmF2L215LW5hdi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0Usd0JBQWdCO0VBQWhCLGdCQUFnQjtFQUNoQixNQUFNO0VBQ04sVUFBVTtBQUNaOztBQUNBO0VBQ0Usa0JBQWtCO0FBQ3BCOztBQUNBO0VBQ0UscUJBQXFCO0VBQ3JCLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIscURBQXFEO0FBQ3ZEOztBQUNBO0VBQ0UseUJBQXlCO0FBQzNCOztBQUNBO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtBQUNwQjs7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtBQUNyQjs7QUFDQSxtQkFBbUI7O0FBRW5COztDQUVDLFdBQVc7Q0FDWCxXQUFXO0NBQ1gsUUFBUTtDQUNSLFVBQVU7Q0FDVixzQkFBc0I7Q0FDdEIsa0JBQWtCO0NBQ2xCLG1CQUFtQjtBQUNwQjs7QUFFQTtDQUNDLFNBQVM7QUFDVjs7QUFFQTtDQUNDLFlBQVk7Q0FDWixRQUFRO0FBQ1Q7O0FBRUE7O0NBRUMsd0JBQXdCO0NBQ3hCLFlBQVk7QUFDYjs7QUFDQTtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsa0JBQWtCO0FBQ3BCOztBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvbXktbmF2L215LW5hdi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNpZGVuYXYtY29udGFpbmVyIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uc2lkZW5hdiB7XG4gIHdpZHRoOiAyMDBweDtcbn1cblxuLnNpZGVuYXYgLm1hdC10b29sYmFyIHtcbiAgYmFja2dyb3VuZDogaW5oZXJpdDtcbn1cblxuLm1hdC10b29sYmFyLm1hdC1wcmltYXJ5IHtcbiAgcG9zaXRpb246IHN0aWNreTtcbiAgdG9wOiAwO1xuICB6LWluZGV4OiAxO1xufVxuaW1nIHtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xufVxuYnV0dG9uIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiBhbGljZWJsdWU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIGZvbnQtZmFtaWx5OiBHZW9yZ2lhLCAnVGltZXMgTmV3IFJvbWFuJywgVGltZXMsIHNlcmlmO1xufVxubWF0LXRvb2xiYXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjQkI1NTQ0O1xufVxuc3BhbiBpbWcge1xuICB3aWR0aDogNTBweDtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xufVxuLm1lbnUtYnV0dG9uIHtcbiAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIHBhZGRpbmctbGVmdDogMzVweDtcbiAgcGFkZGluZy1yaWdodDogMzVweDtcbn1cbi8qIEhPVkVSIEVGRkVDVCAzICovXG5cbi5lZmZlY3QtMyBhOmJlZm9yZSxcbi5lZmZlY3QtMyBhOmFmdGVyIHtcblx0Y29udGVudDogXCJcIjtcblx0aGVpZ2h0OiAxcHg7XG5cdHdpZHRoOiAwO1xuXHRvcGFjaXR5OiAwO1xuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuXHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdHRyYW5zaXRpb246IGFsbCAuNXM7XG59XG5cbi5lZmZlY3QtMyBhOmJlZm9yZSB7XG5cdHRvcDogLTNweDtcbn1cblxuLmVmZmVjdC0zIGE6YWZ0ZXIge1xuXHRib3R0b206IC0zcHg7XG5cdHJpZ2h0OiAwO1xufVxuXG4uZWZmZWN0LTMgYTpob3ZlcjpiZWZvcmUsXG4uZWZmZWN0LTMgYTpob3ZlcjphZnRlciB7XG5cdHdpZHRoOiBjYWxjKDEwMCUgKyAyMHB4KTtcblx0b3BhY2l0eTogMC43O1xufVxuLnByb2ZpbGUtYnV0dG9uIHtcbiAgd2lkdGg6IDUwcHg7XG4gIHJpZ2h0OiAxMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4ubG9nLWljb24ge1xuICBwYWRkaW5nLXRvcDogOHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/my-nav/my-nav.component.ts":
/*!********************************************!*\
  !*** ./src/app/my-nav/my-nav.component.ts ***!
  \********************************************/
/*! exports provided: MyNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyNavComponent", function() { return MyNavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm2015/layout.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _service_student_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/student-service */ "./src/app/service/student-service.ts");
/* harmony import */ var _authentication_authentication_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../authentication/authentication-service.service */ "./src/app/authentication/authentication-service.service.ts");
/* harmony import */ var _user_profile_dialogue_user_profile_dialogue_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./user-profile-dialogue/user-profile-dialogue.component */ "./src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component.ts");








let MyNavComponent = class MyNavComponent {
    constructor(breakpointObserver, studentService, authService, dialog) {
        this.breakpointObserver = breakpointObserver;
        this.studentService = studentService;
        this.authService = authService;
        this.dialog = dialog;
        this.defaultImageUrl = 'assets/images/camt.jpg';
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(result => result.matches), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["shareReplay"])());
        this.students$ = this.studentService.getStudents();
    }
    hasRole(role) {
        return this.authService.hasRole(role);
    }
    get user() {
        return this.authService.getCurrentUser();
    }
    openDialog() {
        const dialogRef = this.dialog.open(_user_profile_dialogue_user_profile_dialogue_component__WEBPACK_IMPORTED_MODULE_7__["UserProfileDialogueComponent"], {
            width: '250px'
        });
    }
};
MyNavComponent.ctorParameters = () => [
    { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["BreakpointObserver"] },
    { type: _service_student_service__WEBPACK_IMPORTED_MODULE_5__["StudentService"] },
    { type: _authentication_authentication_service_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationServiceService"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] }
];
MyNavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-my-nav',
        template: __webpack_require__(/*! raw-loader!./my-nav.component.html */ "./node_modules/raw-loader/index.js!./src/app/my-nav/my-nav.component.html"),
        styles: [__webpack_require__(/*! ./my-nav.component.css */ "./src/app/my-nav/my-nav.component.css")]
    })
], MyNavComponent);



/***/ }),

/***/ "./src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "img {\n    width: auto;\n    padding: 10px;\n    margin-left: 20px;\n}\nbutton {\n    color: white;\n    background-color: #BB5544;\n    align-self: center;\n}\nbutton:hover {\n    background-color: aliceblue;\n    color: #BB5544;\n}\n.button-logout {\n    align-self: center;\n    margin-left: 50px;\n    margin-top: 20px;\n}\n.user-name {\n    margin-bottom: 10px;\n    margin-top: 10px;\n    align-self: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXktbmF2L3VzZXItcHJvZmlsZS1kaWFsb2d1ZS91c2VyLXByb2ZpbGUtZGlhbG9ndWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFdBQVc7SUFDWCxhQUFhO0lBQ2IsaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksMkJBQTJCO0lBQzNCLGNBQWM7QUFDbEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL215LW5hdi91c2VyLXByb2ZpbGUtZGlhbG9ndWUvdXNlci1wcm9maWxlLWRpYWxvZ3VlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbWcge1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5idXR0b24ge1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjQkI1NTQ0O1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cbmJ1dHRvbjpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogYWxpY2VibHVlO1xuICAgIGNvbG9yOiAjQkI1NTQ0O1xufVxuLmJ1dHRvbi1sb2dvdXQge1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICBtYXJnaW4tbGVmdDogNTBweDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuLnVzZXItbmFtZSB7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component.ts ***!
  \*********************************************************************************/
/*! exports provided: UserProfileDialogueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfileDialogueComponent", function() { return UserProfileDialogueComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var src_app_authentication_authentication_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/authentication/authentication-service.service */ "./src/app/authentication/authentication-service.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_service_data_sharing_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/data-sharing.service */ "./src/app/service/data-sharing.service.ts");






let UserProfileDialogueComponent = class UserProfileDialogueComponent {
    constructor(dialogRef, data, authService, router, dataSharingService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.authService = authService;
        this.router = router;
        this.dataSharingService = dataSharingService;
        this.defaultImageUrl = 'assets/images/camt.jpg';
        this.dataSharingService.isUserLoggedIn.subscribe(value => {
            this.isUserLoggedIn = value;
        });
    }
    ngOnInit() {
    }
    hasRole(role) {
        return this.authService.hasRole(role);
    }
    get user() {
        return this.authService.getCurrentUser();
    }
    getUserRole() {
        const userRole = this.user.authorities.map(author => author.name);
        if (userRole == 'ROLE_STUDENT') {
            this.currentRole = 'student';
            return this.currentRole;
        }
        else if (userRole[0] == 'ROLE_LECTURER' || userRole[1] == 'ROLE_STUDENT') {
            this.currentRole = 'lecturer';
            return this.currentRole;
        }
        return this.currentRole;
    }
    userLogout() {
        this.router.navigate(['/login']);
        this.dataSharingService.isUserLoggedIn.next(false);
        return this.authService.logout();
    }
};
UserProfileDialogueComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] },
    { type: src_app_authentication_authentication_service_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationServiceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_service_data_sharing_service__WEBPACK_IMPORTED_MODULE_5__["DataSharingService"] }
];
UserProfileDialogueComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-user-profile-dialogue',
        template: __webpack_require__(/*! raw-loader!./user-profile-dialogue.component.html */ "./node_modules/raw-loader/index.js!./src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component.html"),
        styles: [__webpack_require__(/*! ./user-profile-dialogue.component.css */ "./src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"]))
], UserProfileDialogueComponent);



/***/ }),

/***/ "./src/app/service/data-sharing.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/data-sharing.service.ts ***!
  \*************************************************/
/*! exports provided: DataSharingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataSharingService", function() { return DataSharingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");



let DataSharingService = class DataSharingService {
    constructor() {
        this.isUserLoggedIn = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](false);
    }
};
DataSharingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], DataSharingService);



/***/ }),

/***/ "./src/app/service/file-upload.service.ts":
/*!************************************************!*\
  !*** ./src/app/service/file-upload.service.ts ***!
  \************************************************/
/*! exports provided: FileUploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUploadService", function() { return FileUploadService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");






let FileUploadService = class FileUploadService {
    constructor(http) {
        this.http = http;
    }
    uploadFile(image) {
        const formData = new FormData();
        formData.append('file', image);
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].uploadApi, formData, {
            reportProgress: true,
            observe: 'events',
            responseType: 'text'
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorMgmt));
    }
    errorMgmt(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        }
        else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
    }
};
FileUploadService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
FileUploadService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])({
        providedIn: 'root'
    })
], FileUploadService);



/***/ }),

/***/ "./src/app/service/student-rest-impl.service.ts":
/*!******************************************************!*\
  !*** ./src/app/service/student-rest-impl.service.ts ***!
  \******************************************************/
/*! exports provided: StudentRestImplService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentRestImplService", function() { return StudentRestImplService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _student_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./student-service */ "./src/app/service/student-service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");





let StudentRestImplService = class StudentRestImplService extends _student_service__WEBPACK_IMPORTED_MODULE_2__["StudentService"] {
    constructor(http) {
        super();
        this.http = http;
    }
    getStudents() {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].studentApi);
    }
    getStudent(id) {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].studentApi + '/' + id);
    }
    saveStudent(student) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].studentApi, student);
    }
};
StudentRestImplService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
StudentRestImplService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], StudentRestImplService);



/***/ }),

/***/ "./src/app/service/student-service.ts":
/*!********************************************!*\
  !*** ./src/app/service/student-service.ts ***!
  \********************************************/
/*! exports provided: StudentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentService", function() { return StudentService; });
class StudentService {
}


/***/ }),

/***/ "./src/app/shared/file-not-found/file-not-found.component.css":
/*!********************************************************************!*\
  !*** ./src/app/shared/file-not-found/file-not-found.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9maWxlLW5vdC1mb3VuZC9maWxlLW5vdC1mb3VuZC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/shared/file-not-found/file-not-found.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shared/file-not-found/file-not-found.component.ts ***!
  \*******************************************************************/
/*! exports provided: FileNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileNotFoundComponent", function() { return FileNotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FileNotFoundComponent = class FileNotFoundComponent {
    constructor() { }
    ngOnInit() {
    }
};
FileNotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-file-not-found',
        template: __webpack_require__(/*! raw-loader!./file-not-found.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/file-not-found/file-not-found.component.html"),
        styles: [__webpack_require__(/*! ./file-not-found.component.css */ "./src/app/shared/file-not-found/file-not-found.component.css")]
    })
], FileNotFoundComponent);



/***/ }),

/***/ "./src/app/students/add/students.add.component.css":
/*!*********************************************************!*\
  !*** ./src/app/students/add/students.add.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-width {\n  width: 100%;\n}\n\n.center-button {\n  text-align: center;\n  padding-left: .5em;\n  min-width: 0.5rem;\n  max-width: 0.5rem;\n}\n\n.center-text {\n  text-align: center;\n}\n\n.center {\n  display: flex;\n  justify-content: center;\n}\n\n.penInput {\n\tmin-width:3rem;\n\tmax-width: 6rem;\n  }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvYWRkL3N0dWRlbnRzLmFkZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0UsYUFBYTtFQUNiLHVCQUF1QjtBQUN6Qjs7QUFDQTtDQUNDLGNBQWM7Q0FDZCxlQUFlO0VBQ2QiLCJmaWxlIjoic3JjL2FwcC9zdHVkZW50cy9hZGQvc3R1ZGVudHMuYWRkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZnVsbC13aWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uY2VudGVyLWJ1dHRvbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAuNWVtO1xuICBtaW4td2lkdGg6IDAuNXJlbTtcbiAgbWF4LXdpZHRoOiAwLjVyZW07XG59XG5cbi5jZW50ZXItdGV4dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNlbnRlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLnBlbklucHV0IHtcblx0bWluLXdpZHRoOjNyZW07XG5cdG1heC13aWR0aDogNnJlbTtcbiAgfVxuIl19 */"

/***/ }),

/***/ "./src/app/students/add/students.add.component.ts":
/*!********************************************************!*\
  !*** ./src/app/students/add/students.add.component.ts ***!
  \********************************************************/
/*! exports provided: StudentsAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsAddComponent", function() { return StudentsAddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_service_file_upload_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/file-upload.service */ "./src/app/service/file-upload.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_service_student_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/service/student-service */ "./src/app/service/student-service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");








let StudentsAddComponent = class StudentsAddComponent {
    constructor(fb, studentService, router, fileUploadService) {
        this.fb = fb;
        this.studentService = studentService;
        this.router = router;
        this.fileUploadService = fileUploadService;
        this.form = this.fb.group({
            id: [''],
            studentId: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(13)])],
            name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            surname: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            gpa: [''],
            image: [''],
            featured: [''],
            penAmount: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('[0-9]+')])],
            description: ['']
        });
        this.validation_messages = {
            'studentId': [
                { type: 'required', message: 'student id is required' },
                { type: 'maxlength', message: 'student id is too long' }
            ],
            'name': [
                { type: 'required', message: 'the name is required' }
            ],
            'surname': [
                { type: 'required', message: 'the surname is required' }
            ],
            'penAmount': [
                { type: 'required', message: 'the penAmount is required' },
                { type: 'pattern', message: 'please enter number' }
            ],
            'image': [],
            'description': []
        };
    }
    ngOnInit() {
        this.uploadEndPoint = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].uploadApi;
    }
    get diagnostic() {
        return JSON.stringify(this.form.value);
    }
    upQuantity(student) {
        this.form.patchValue({
            penAmount: +this.form.value['penAmount'] + 1
        });
    }
    downQuantity(student) {
        if (+this.form.value['penAmount'] > 0) {
            this.form.patchValue({
                penAmount: +this.form.value['penAmount'] - 1
            });
        }
    }
    submit() {
        this.studentService.saveStudent(this.form.value)
            .subscribe((student) => {
            this.router.navigate(['/detail/', student.id]);
        }, (error) => {
            alert('could not save value');
        });
    }
    onUploadClicked(files) {
        console.log(typeof (files));
        console.log(files.item(0));
        const uploadedFile = files.item(0);
        this.progress = 0;
        this.fileUploadService.uploadFile(uploadedFile)
            .subscribe((event) => {
            switch (event.type) {
                case _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].Sent:
                    console.log('Request has been made!');
                    break;
                case _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].ResponseHeader:
                    console.log('Response header has been received!');
                    break;
                case _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].UploadProgress:
                    this.progress = Math.round(event.loaded / event.total * 100);
                    console.log(`Uploaded! ${this.progress}%`);
                    break;
                case _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].Response:
                    console.log('User successfully created!', event.body);
                    this.uploadedUrl = event.body;
                    this.form.patchValue({
                        image: this.uploadedUrl
                    });
                    this.form.get('image').updateValueAndValidity();
                    setTimeout(() => {
                        this.progress = 0;
                    }, 1500);
            }
        });
    }
    onSelectedFilesChanged(files) {
    }
};
StudentsAddComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: src_app_service_student_service__WEBPACK_IMPORTED_MODULE_6__["StudentService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_app_service_file_upload_service__WEBPACK_IMPORTED_MODULE_4__["FileUploadService"] }
];
StudentsAddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-students-add',
        template: __webpack_require__(/*! raw-loader!./students.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/add/students.add.component.html"),
        styles: [__webpack_require__(/*! ./students.add.component.css */ "./src/app/students/add/students.add.component.css")]
    })
], StudentsAddComponent);



/***/ }),

/***/ "./src/app/students/list/students.component.css":
/*!******************************************************!*\
  !*** ./src/app/students/list/students.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".student-name{\n\t\tcolor: #FF0000;\n\t\tfont-size: x-large;\n\t}\n.grade {\n\t\tfont-weight: bold;\n\t}\n.featured{\n\tbackground: linear-gradient(to bottom right, red, yellow);\n}\nbutton{\n\tbox-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);\n\tmargin-top: 2px;\n}\ninput{\n\tbox-sizing: border-box;\n    border: 2px solid #ccc;\n    border-radius: 4px;\n\tmargin-left: 2px;\n\tmargin-right: 2px;\n\twidth: 60px;\n\ttext-align: center;\n\n}\n\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvbGlzdC9zdHVkZW50cy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtDQUNuQjtBQUNEO0VBQ0UsaUJBQWlCO0NBQ2xCO0FBQ0Q7Q0FDQyx5REFBeUQ7QUFDMUQ7QUFDQTtDQUNDLHVFQUF1RTtDQUN2RSxlQUFlO0FBQ2hCO0FBRUE7Q0FDQyxzQkFBc0I7SUFDbkIsc0JBQXNCO0lBQ3RCLGtCQUFrQjtDQUNyQixnQkFBZ0I7Q0FDaEIsaUJBQWlCO0NBQ2pCLFdBQVc7Q0FDWCxrQkFBa0I7O0FBRW5CIiwiZmlsZSI6InNyYy9hcHAvc3R1ZGVudHMvbGlzdC9zdHVkZW50cy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN0dWRlbnQtbmFtZXtcblx0XHRjb2xvcjogI0ZGMDAwMDtcblx0XHRmb250LXNpemU6IHgtbGFyZ2U7XG5cdH1cbi5ncmFkZSB7XG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG5cdH1cbi5mZWF0dXJlZHtcblx0YmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSByaWdodCwgcmVkLCB5ZWxsb3cpO1xufVxuYnV0dG9ue1xuXHRib3gtc2hhZG93OiAwIDhweCAxNnB4IDAgcmdiYSgwLDAsMCwwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLDAsMCwwLjE5KTtcblx0bWFyZ2luLXRvcDogMnB4O1xufVxuXG5pbnB1dHtcblx0Ym94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjY2NjO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcblx0bWFyZ2luLWxlZnQ6IDJweDtcblx0bWFyZ2luLXJpZ2h0OiAycHg7XG5cdHdpZHRoOiA2MHB4O1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbn1cblxuXG4iXX0= */"

/***/ }),

/***/ "./src/app/students/list/students.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/students/list/students.component.ts ***!
  \*****************************************************/
/*! exports provided: StudentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsComponent", function() { return StudentsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _service_student_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/student-service */ "./src/app/service/student-service.ts");



let StudentsComponent = class StudentsComponent {
    constructor(studentService) {
        this.studentService = studentService;
    }
    ngOnInit() {
        this.studentService.getStudents()
            .subscribe(students => this.students = students);
    }
    averageGpa() {
        let sum = 0;
        if (Array.isArray(this.students)) {
            for (const student of this.students) {
                sum += student.gpa;
            }
            return sum / this.students.length;
        }
        else {
            return null;
        }
    }
    upQuantity(student) {
        student.penAmount++;
    }
    downQuantity(student) {
        if (student.penAmount > 0) {
            student.penAmount--;
        }
    }
};
StudentsComponent.ctorParameters = () => [
    { type: _service_student_service__WEBPACK_IMPORTED_MODULE_2__["StudentService"] }
];
StudentsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-students',
        template: __webpack_require__(/*! raw-loader!./students.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/list/students.component.html"),
        styles: [__webpack_require__(/*! ./students.component.css */ "./src/app/students/list/students.component.css")]
    })
], StudentsComponent);



/***/ }),

/***/ "./src/app/students/student-routing.module.ts":
/*!****************************************************!*\
  !*** ./src/app/students/student-routing.module.ts ***!
  \****************************************************/
/*! exports provided: StudentRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentRoutingModule", function() { return StudentRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _view_students_view_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view/students.view.component */ "./src/app/students/view/students.view.component.ts");
/* harmony import */ var _add_students_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/students.add.component */ "./src/app/students/add/students.add.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _student_table_student_table_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./student-table/student-table.component */ "./src/app/students/student-table/student-table.component.ts");
/* harmony import */ var _Guard_lecturer_guard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Guard/lecturer-guard */ "./src/app/Guard/lecturer-guard.ts");
/* harmony import */ var _Guard_lecturer_and_student_guard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Guard/lecturer-and-student-guard */ "./src/app/Guard/lecturer-and-student-guard.ts");








const StudentRoutes = [
    { path: 'add', component: _add_students_add_component__WEBPACK_IMPORTED_MODULE_3__["StudentsAddComponent"], canActivate: [_Guard_lecturer_guard__WEBPACK_IMPORTED_MODULE_6__["LecturerGuard"]] },
    { path: 'list', component: _student_table_student_table_component__WEBPACK_IMPORTED_MODULE_5__["StudentTableComponent"], canActivate: [_Guard_lecturer_and_student_guard__WEBPACK_IMPORTED_MODULE_7__["StudentAndLecturerGuard"]] },
    { path: 'detail/:id', component: _view_students_view_component__WEBPACK_IMPORTED_MODULE_2__["StudentsViewComponent"], canActivate: [_Guard_lecturer_and_student_guard__WEBPACK_IMPORTED_MODULE_7__["StudentAndLecturerGuard"]] },
    { path: 'table', component: _student_table_student_table_component__WEBPACK_IMPORTED_MODULE_5__["StudentTableComponent"], canActivate: [_Guard_lecturer_and_student_guard__WEBPACK_IMPORTED_MODULE_7__["StudentAndLecturerGuard"]] }
];
let StudentRoutingModule = class StudentRoutingModule {
};
StudentRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(StudentRoutes)
        ],
        exports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
        ]
    })
], StudentRoutingModule);



/***/ }),

/***/ "./src/app/students/student-table/student-table-datasource.ts":
/*!********************************************************************!*\
  !*** ./src/app/students/student-table/student-table-datasource.ts ***!
  \********************************************************************/
/*! exports provided: StudentTableDataSource */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentTableDataSource", function() { return StudentTableDataSource; });
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm2015/collections.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");



// TODO: replace this with real data from your application
const EXAMPLE_DATA = [];
/**
 * Data source for the StudentTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
class StudentTableDataSource extends _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_0__["DataSource"] {
    constructor() {
        super();
        this.data = EXAMPLE_DATA;
    }
    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect() {
        // Combine everything that affects the rendered data into one update
        // stream for the data-table to consume.
        const dataMutations = [
            Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.data),
            this.paginator.page,
            this.sort.sortChange,
            this.filter$.asObservable()
        ];
        // Set the paginators length
        this.paginator.length = this.data.length;
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(...dataMutations).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(() => {
            return this.getFilter(this.getPagedData(this.getSortedData([...this.data])));
        }));
    }
    /**
     *  Called when the table is being destroyed. Use this function, to clean up
     * any open connections or free any held resources that were set up during connect.
     */
    disconnect() { }
    /**
     * Paginate the data (client-side). If you're using server-side pagination,
     * this would be replaced by requesting the appropriate data from the server.
     */
    getPagedData(data) {
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        return data.splice(startIndex, this.paginator.pageSize);
    }
    /**
     * Sort the data (client-side). If you're using server-side sorting,
     * this would be replaced by requesting the appropriate data from the server.
     */
    getSortedData(data) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }
        return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'name': return compare(a.name, b.name, isAsc);
                case 'id': return compare(+a.id, +b.id, isAsc);
                case 'surname': return compare(a.surname, b.surname, isAsc);
                default: return 0;
            }
        });
    }
    // load data from the user
    getFilter(data) {
        const filter = this.filter$.getValue();
        if (filter === '') {
            return data;
        }
        return data.filter((student) => {
            return (student.name.toLowerCase().includes(filter) || student.surname.toLowerCase().includes(filter));
        });
    }
}
/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}


/***/ }),

/***/ "./src/app/students/student-table/student-table.component.css":
/*!********************************************************************!*\
  !*** ./src/app/students/student-table/student-table.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* Structure */\n\ntable {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\nimg {\n    max-width: 20rem;\n    max-height: 20rem;\n}\n\ntr.mat-footer-row {\n    font-weight: bold;\n}\n\n.penInput {\n    min-width: 2rem;\n    max-width: 2rem;\n}\n\n.mat-cell {\n    padding: 8px 8px 8px 0;\n}\n\n.mat-raised-button {\n    min-width: 0.5rem;\n    max-width: 0.5rem;\n}\n\n.center-button {\n    text-align: center;\n    padding-left: .5em;\n}\n\n.center-text {\n    text-align: center;\n}\n\n.center {\n    display: flex;\n    justify-content: center;\n}\n\n/* Absolute Center Spinner */\n\n.loading-indicator {\n    position: fixed;\n    z-index: 999;\n    height: 2em;\n    width: 2em;\n    overflow: show;\n    margin: auto;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    right: 0;\n}\n\n/* Transparent Overlay */\n\n.loading-indicator:before {\n    content: '';\n    display: block;\n    position: fixed;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-color: rgba(0, 0, 0, 0.3);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvc3R1ZGVudC10YWJsZS9zdHVkZW50LXRhYmxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsY0FBYzs7QUFFZDtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGVBQWU7SUFDZixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGFBQWE7SUFDYix1QkFBdUI7QUFDM0I7O0FBR0EsNEJBQTRCOztBQUU1QjtJQUNJLGVBQWU7SUFDZixZQUFZO0lBQ1osV0FBVztJQUNYLFVBQVU7SUFDVixjQUFjO0lBQ2QsWUFBWTtJQUNaLE1BQU07SUFDTixPQUFPO0lBQ1AsU0FBUztJQUNULFFBQVE7QUFDWjs7QUFHQSx3QkFBd0I7O0FBRXhCO0lBQ0ksV0FBVztJQUNYLGNBQWM7SUFDZCxlQUFlO0lBQ2YsTUFBTTtJQUNOLE9BQU87SUFDUCxXQUFXO0lBQ1gsWUFBWTtJQUNaLG9DQUFvQztBQUN4QyIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnRzL3N0dWRlbnQtdGFibGUvc3R1ZGVudC10YWJsZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogU3RydWN0dXJlICovXG5cbnRhYmxlIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbmltZyB7XG4gICAgbWF4LXdpZHRoOiAyMHJlbTtcbiAgICBtYXgtaGVpZ2h0OiAyMHJlbTtcbn1cblxudHIubWF0LWZvb3Rlci1yb3cge1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ucGVuSW5wdXQge1xuICAgIG1pbi13aWR0aDogMnJlbTtcbiAgICBtYXgtd2lkdGg6IDJyZW07XG59XG5cbi5tYXQtY2VsbCB7XG4gICAgcGFkZGluZzogOHB4IDhweCA4cHggMDtcbn1cblxuLm1hdC1yYWlzZWQtYnV0dG9uIHtcbiAgICBtaW4td2lkdGg6IDAuNXJlbTtcbiAgICBtYXgtd2lkdGg6IDAuNXJlbTtcbn1cblxuLmNlbnRlci1idXR0b24ge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nLWxlZnQ6IC41ZW07XG59XG5cbi5jZW50ZXItdGV4dCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uY2VudGVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG5cbi8qIEFic29sdXRlIENlbnRlciBTcGlubmVyICovXG5cbi5sb2FkaW5nLWluZGljYXRvciB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHotaW5kZXg6IDk5OTtcbiAgICBoZWlnaHQ6IDJlbTtcbiAgICB3aWR0aDogMmVtO1xuICAgIG92ZXJmbG93OiBzaG93O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICBib3R0b206IDA7XG4gICAgcmlnaHQ6IDA7XG59XG5cblxuLyogVHJhbnNwYXJlbnQgT3ZlcmxheSAqL1xuXG4ubG9hZGluZy1pbmRpY2F0b3I6YmVmb3JlIHtcbiAgICBjb250ZW50OiAnJztcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC4zKTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/students/student-table/student-table.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/students/student-table/student-table.component.ts ***!
  \*******************************************************************/
/*! exports provided: StudentTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentTableComponent", function() { return StudentTableComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm2015/paginator.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm2015/sort.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm2015/table.js");
/* harmony import */ var _student_table_datasource__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./student-table-datasource */ "./src/app/students/student-table/student-table-datasource.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var src_app_service_student_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/service/student-service */ "./src/app/service/student-service.ts");








let StudentTableComponent = class StudentTableComponent {
    constructor(studentService) {
        this.studentService = studentService;
        /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
        this.displayedColumns = ['id', 'studentId', 'name', 'surname', 'image', 'penAmount', 'gpa'];
    }
    ngOnInit() {
        this.loading = true;
        this.studentService.getStudents()
            .subscribe(students => {
            this.dataSource = new _student_table_datasource__WEBPACK_IMPORTED_MODULE_5__["StudentTableDataSource"]();
            this.dataSource.data = students;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.table.dataSource = this.dataSource;
            this.students = students;
            this.filter$ = new rxjs__WEBPACK_IMPORTED_MODULE_6__["BehaviorSubject"]('');
            this.dataSource.filter$ = this.filter$;
            setTimeout(() => {
                this.loading = false;
            }, 500);
        });
    }
    ngAfterViewInit() {
    }
    applyFilter(filterValue) {
        this.filter$.next(filterValue.trim().toLowerCase());
    }
    averageGpa() {
        let sum = 0;
        if (Array.isArray(this.students)) {
            for (const student of this.students) {
                sum += student.gpa;
            }
            return sum / this.students.length;
        }
        else {
            return null;
        }
    }
    upQuantity(student) {
        student.penAmount++;
    }
    downQuantity(student) {
        if (student.penAmount > 0) {
            student.penAmount--;
        }
    }
};
StudentTableComponent.ctorParameters = () => [
    { type: src_app_service_student_service__WEBPACK_IMPORTED_MODULE_7__["StudentService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"], { static: false })
], StudentTableComponent.prototype, "paginator", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_sort__WEBPACK_IMPORTED_MODULE_3__["MatSort"], { static: false })
], StudentTableComponent.prototype, "sort", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_table__WEBPACK_IMPORTED_MODULE_4__["MatTable"], { static: false })
], StudentTableComponent.prototype, "table", void 0);
StudentTableComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-student-table',
        template: __webpack_require__(/*! raw-loader!./student-table.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/student-table/student-table.component.html"),
        styles: [__webpack_require__(/*! ./student-table.component.css */ "./src/app/students/student-table/student-table.component.css")]
    })
], StudentTableComponent);



/***/ }),

/***/ "./src/app/students/view/students.view.component.css":
/*!***********************************************************!*\
  !*** ./src/app/students/view/students.view.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-width {\n\twidth: 100%;\n  }\n  \n  .center-button {\n\ttext-align: center;\n\tpadding-left: .5em;\n\tmin-width: 0.5rem;\n\tmax-width: 0.5rem;\n  }\n  \n  .center-text {\n\ttext-align: center;\n  }\n  \n  .center {\n\tdisplay: flex;\n\tjustify-content: center;\n  }\n  \n  .penInput {\n\t  min-width:3rem;\n\t  max-width: 6rem;\n\t}\n  \n  .img-width{\n\t\tmax-width: 400px;\n\t\t\n\t}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvdmlldy9zdHVkZW50cy52aWV3LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Q0FDQyxXQUFXO0VBQ1Y7O0VBRUE7Q0FDRCxrQkFBa0I7Q0FDbEIsa0JBQWtCO0NBQ2xCLGlCQUFpQjtDQUNqQixpQkFBaUI7RUFDaEI7O0VBRUE7Q0FDRCxrQkFBa0I7RUFDakI7O0VBRUE7Q0FDRCxhQUFhO0NBQ2IsdUJBQXVCO0VBQ3RCOztFQUNBO0dBQ0MsY0FBYztHQUNkLGVBQWU7Q0FDakI7O0VBRUE7RUFDQyxnQkFBZ0I7O0NBRWpCIiwiZmlsZSI6InNyYy9hcHAvc3R1ZGVudHMvdmlldy9zdHVkZW50cy52aWV3LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZnVsbC13aWR0aCB7XG5cdHdpZHRoOiAxMDAlO1xuICB9XG4gIFxuICAuY2VudGVyLWJ1dHRvbiB7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcblx0cGFkZGluZy1sZWZ0OiAuNWVtO1xuXHRtaW4td2lkdGg6IDAuNXJlbTtcblx0bWF4LXdpZHRoOiAwLjVyZW07XG4gIH1cbiAgXG4gIC5jZW50ZXItdGV4dCB7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICBcbiAgLmNlbnRlciB7XG5cdGRpc3BsYXk6IGZsZXg7XG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB9XG4gIC5wZW5JbnB1dCB7XG5cdCAgbWluLXdpZHRoOjNyZW07XG5cdCAgbWF4LXdpZHRoOiA2cmVtO1xuXHR9XG5cblx0LmltZy13aWR0aHtcblx0XHRtYXgtd2lkdGg6IDQwMHB4O1xuXHRcdFxuXHR9Il19 */"

/***/ }),

/***/ "./src/app/students/view/students.view.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/students/view/students.view.component.ts ***!
  \**********************************************************/
/*! exports provided: StudentsViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsViewComponent", function() { return StudentsViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_service_student_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/student-service */ "./src/app/service/student-service.ts");




let StudentsViewComponent = class StudentsViewComponent {
    constructor(route, studentService) {
        this.route = route;
        this.studentService = studentService;
    }
    ngOnInit() {
        this.route.params
            .subscribe((params) => {
            this.studentService.getStudent(+params['id'])
                .subscribe((inputStudent) => this.student = inputStudent);
        });
    }
};
StudentsViewComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_service_student_service__WEBPACK_IMPORTED_MODULE_3__["StudentService"] }
];
StudentsViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-students-view',
        template: __webpack_require__(/*! raw-loader!./students.view.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/view/students.view.component.html"),
        styles: [__webpack_require__(/*! ./students.view.component.css */ "./src/app/students/view/students.view.component.css")]
    })
], StudentsViewComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    studentApi: 'http://localhost:8080/students',
    uploadApi: 'http://localhost:8080/uploadFile',
    //uploadApi: 'http://localhost:8080/uploadFile',
    authenticationApi: 'http://localhost:8080/auth'
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/nebdara/se331-project-frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map