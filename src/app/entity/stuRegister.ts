import User from '../entity/user';

export default class StuRegister {
  id: number;
  firstname: string;
  lastname: string;
  image: string;
  dob: string;
  studentId: string;
  username: string;
  password: string;
  email: string;
  lastPasswordResetDate: string;
  authorities: string;
  gpa: number;
}
