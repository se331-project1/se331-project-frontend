export default class Comment {
  id: number;
  commentContent: string;
  commentor: string;
  commentedDate: string;
  commentorImg: string;
}
