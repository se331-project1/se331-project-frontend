export default class User {
  id: number;
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  email: string;
  enabled: boolean;
  image: string;
  lastPasswordResetDate: string;
  authorities: string;
  appUser: User;
}
