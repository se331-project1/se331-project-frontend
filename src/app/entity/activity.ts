export default class Activity {
  id: number;
  activityDate: Date;
  activityDescription: string;
  activityLocation: string;
  activityName: string;
  activityId: string;
  hostTeacher: string;
  periodRegistration: string;

  // activityTime: string;
  // Status: String;
}
