import { Component, OnInit, Inject } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AuthenticationServiceService } from "src/app/authentication/authentication-service.service";
import { Router } from "@angular/router";
import { DataSharingService } from "src/app/service/data-sharing.service";
export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: "app-user-profile-dialogue",
  templateUrl: "./user-profile-dialogue.component.html",
  styleUrls: ["./user-profile-dialogue.component.css"]
})
export class UserProfileDialogueComponent implements OnInit {
  defaultImageUrl = "assets/images/camt.jpg";
  currentRole: string;
  isUserLoggedIn: boolean;
  cUser = this.authService.getCurrentUser();

  constructor(
    public dialogRef: MatDialogRef<UserProfileDialogueComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService: AuthenticationServiceService,
    private router: Router,
    private dataSharingService: DataSharingService
  ) {
    this.dataSharingService.isUserLoggedIn.subscribe(value => {
      this.isUserLoggedIn = value;
    });
  }

  ngOnInit() {}
  hasRole(role: string) {
    return this.authService.hasRole(role);
  }

  get user() {
    return this.authService.getCurrentUser();
  }
  getUserName() {
    return this.cUser.firstname;
  }

  getUserRole() {
    const userRole = this.user.authorities.map(author => author.name);
    if (userRole == "ROLE_STUDENT") {
      return "Student";
    } else if (userRole == "ROLE_ADMINISTRATOR") {
      return "Adminnistrator";
    } else if (userRole[0] == "ROLE_LECTURER" && userRole[1] == "ROLE_STUDENT") {
      return "Lecturer";
    }
    return this.currentRole;
  }

  userLogout() {
    this.router.navigate(["/login"]);
    this.dataSharingService.isUserLoggedIn.next(false);
    return this.authService.logout();
  }
}
