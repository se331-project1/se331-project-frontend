import { Component } from "@angular/core";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Observable } from "rxjs";
import { map, shareReplay } from "rxjs/operators";
import Student from "../entity/student";
import { StudentService } from "../service/student-service";
import { AuthenticationServiceService } from "../authentication/authentication-service.service";
import { UserProfileDialogueComponent } from "./user-profile-dialogue/user-profile-dialogue.component";

@Component({
  selector: "app-my-nav",
  templateUrl: "./my-nav.component.html",
  styleUrls: ["./my-nav.component.css"]
})
export class MyNavComponent {
  defaultImageUrl = "assets/images/camt.jpg";
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(result => result.matches),
    shareReplay()
  );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private studentService: StudentService,
    private authService: AuthenticationServiceService,
    public dialog: MatDialog
  ) {}
  students$: Observable<Student[]> = this.studentService.getStudents();
  hasRole(role: string) {
    return this.authService.hasRole(role);
  }
  get user() {
    return this.authService.getCurrentUser();
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(UserProfileDialogueComponent, {
      width: "520px"
    });
  }
}
