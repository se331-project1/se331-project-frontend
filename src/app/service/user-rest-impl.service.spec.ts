import { TestBed } from '@angular/core/testing';

import { UserRestImplService } from './user-rest-impl.service';

describe('UserRestImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserRestImplService = TestBed.get(UserRestImplService);
    expect(service).toBeTruthy();
  });
});
