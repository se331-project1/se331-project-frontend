import { Injectable } from "@angular/core";
import { LecturerServiceService } from "./lecturer-service.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import Lecturer from "../entity/lecturer";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class LecturerRestImplService extends LecturerServiceService {
  constructor(private http: HttpClient) {
    super();
  }
  getLectures(): Observable<Lecturer[]> {
    const headers = new HttpHeaders({ "Content-Type": "application/json;charset=UTF-8" });
    return this.http.get<Lecturer[]>(environment.lecturerApi, { headers });
  }
}
