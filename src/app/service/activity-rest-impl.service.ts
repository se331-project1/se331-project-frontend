import { Injectable } from "@angular/core";
import { ActivityService } from "./activity-service";
import Activity from "../entity/activity";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class ActivityRestImplService extends ActivityService {
  constructor(private http: HttpClient) {
    super();
  }

  enrollActivity(activity: Activity): Observable<Activity> {
    throw new Error("Method not implemented.");
  }
  getEnrollActivity(): Observable<Activity[]> {
    throw new Error("Method not implemented.");
  }

  getActivities(): Observable<Activity[]> {
    const headers = new HttpHeaders({ "Content-Type": "application/json;charset=UTF-8" });
    return this.http.get<Activity[]>(environment.activityApi, { headers });
  }

  getActivity(id: number): Observable<Activity> {
    const headers = new HttpHeaders({ "Content-Type": "application/json;charset=UTF-8" });
    return this.http.get<Activity>(environment.activityApi + "/" + id, { headers });
  }

  saveActivity(activity: Activity): Observable<Activity> {
    const headers = new HttpHeaders({ "Content-Type": "application/json;charset=UTF-8" });
    return this.http.post<Activity>(environment.activityApi, activity, { headers });
  }
  // enrollActivity(activity: Activity): Observable<Activity> {
  //   return this.http.post<Activity>(environment.EnrollmentApi,activity);
  // }
  // getEnrollActivity(): Observable<Activity[]>{
  //   return this.http.get<Activity[]>(environment.EnrollmentApi);
  // };
  updateActivity(id: number, activity: Activity): Observable<Activity> {
    const headers = new HttpHeaders({ "Content-Type": "application/json;charset=UTF-8" });
    return this.http.put<Activity>(environment.activityApi + "/" + id, activity, { headers });
  }
}
