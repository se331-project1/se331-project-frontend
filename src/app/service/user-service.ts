
import User from '../entity/user';
import { Observable } from 'rxjs';

export abstract class UserService {
     abstract getUsers(): Observable<User[]>;
     abstract getUser(id: number): Observable<User>;
     abstract updateUser(user: User): Observable<User>;
     abstract load();
}
