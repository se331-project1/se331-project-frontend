import { Injectable } from "@angular/core";
import { UserService } from "./user-service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import User from "../entity/user";

@Injectable({
  providedIn: "root"
})
export class UserRestImplService extends UserService {
  constructor(private http: HttpClient) {
    super();
  }

  getUsers(): Observable<User[]> {
    const headers = new HttpHeaders({ "Content-Type": "application/json;charset=UTF-8" });
    return this.http.get<User[]>(environment.updateProfileApi, { headers });
  }
  getUser(id: number): Observable<User> {
    const headers = new HttpHeaders({ "Content-Type": "application/json;charset=UTF-8" });
    return this.http.get<User>(environment.updateProfileApi + "/" + id, { headers });
  }
  updateUser(user: User): Observable<User> {
    const headers = new HttpHeaders({ "Content-Type": "application/json;charset=UTF-8" });
    return this.http.put<User>(environment.updateProfileApi, user, { headers });
  }
  load() {}
}
