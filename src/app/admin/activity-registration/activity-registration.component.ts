import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';
import Activity from 'src/app/entity/activity';
import Lecturer from 'src/app/entity/lecturer';
import { LecturerServiceService } from 'src/app/service/lecturer-service.service';
import { FormControl } from '@angular/forms';



@Component({
  selector: 'app-activity-registration',
  templateUrl: './activity-registration.component.html',
  styleUrls: ['./activity-registration.component.css']
})

export class ActivityRegistrationComponent implements OnInit {
  options: string[] = ['CAMT', 'SMO', 'ROOM114', 'ROOM112'];

  addressForm = this.fb.group({
    activityTime: [null, Validators.required],
    activityLocation: [null, Validators.required],
    activityDate: [null, Validators.required],
    periodRegistration: [null, Validators.required],
    activityDescription: [null, Validators.required],
    lecturers: [null, Validators.required]
  });

  lecturers: Lecturer[] = [];
  activities: Activity[];
  validation_messages = {
    'activityName': [
      { type: 'required', message: 'Activity Name is <strong>required</strong>' },
    ],
    'activityTime': [
      { type: 'required', message: 'Activity Time is <strong>required</strong>' },
    ],
    'activityLocation': [
      { type: 'required', message: 'Activity Location is <strong>required</strong>' }
    ],
    'activityDate': [
      { type: 'required', message: 'Activity Date is <strong>required</strong>' }
    ]
    ,
    'periodRegistration': [
      { type: 'required', message: 'Period Registration is <strong>required</strong>' },
    ]
    ,
    'activityDescription': [
      { type: 'required', message: 'Activity Description is <strong>required</strong>' }
    ]
    ,
    'lecturers': [
      { type: 'required', message: 'Lecturer is <strong>required</strong>' }
    ]
  };
  constructor(private fb: FormBuilder, private lecturerService: LecturerServiceService,
    private activityService: ActivityService, private router: Router) {
  }

  form = this.fb.group({
    activityName: [''],
    activityTime: [''],
    activityDate: [''],
    activityLocation: [''],
    periodRegistration: [''],
    activityDescription: [''],
    hostTeacher: [''],
    id: [''],
    lecturers: [''],
    Status: ['Pending']
  })
  ngOnInit(): void {
    this.lecturerService.getLectures().subscribe(
      lecturers => {
        this.lecturers = lecturers;
      }
    );
  }

  submit() {
    // this.activityService.saveActivity(this.form.value)
    //   .subscribe((activity) => {
    //     this.router.navigate(['./admin/registered']);
    //     console.log(this.form.value);

    //   }, (error) => {
    //     alert('could not save the value' + error)
    //   }
    //   )
    alert('Update successfully');
    this.router.navigate(['./list']);

  }



}
