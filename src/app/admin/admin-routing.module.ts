import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ActivityRegistrationComponent } from './activity-registration/activity-registration.component';
import { StudentsViewComponent } from '../students/view/students.view.component';
import { LoginComponent } from '../login/login.component';




const AdminRoutes: Routes = [
 
            { path : 'activities', component : ActivityRegistrationComponent}

];
@NgModule({
    imports: [
        RouterModule.forRoot(AdminRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AdminRoutingModule {
    
}
