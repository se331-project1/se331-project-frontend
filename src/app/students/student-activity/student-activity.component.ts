import { Component, OnInit, AfterViewInit, ViewChild } from "@angular/core";
// import { MatPaginator, MatSort, MatTable } from '@angular/material';
import { MatPaginator } from "@angular/material";
import { MatSort } from "@angular/material/sort";
import { MatTable } from "@angular/material/table";
import { ActivityTableDataSource, ActivityTableItem } from "./student-activity-datasource";
import { ActivityService } from "src/app/service/activity-service";
import { Router } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AuthenticationServiceService } from 'src/app/authentication/authentication-service.service';

// import Activity from 'src/app/entity/activity';
// import { ActivityTableDataSource } from 'src/app/students/student-activity/student-activity-datasource';
// import { BehaviorSubject } from 'rxjs';
// import { ActivityService } from 'src/app/service/activity-service';
// import { Router } from '@angular/router';
// import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: "app-student-activity",
  templateUrl: "./student-activity.component.html",
  styleUrls: ["./student-activity.component.css"]
})
export class StudentActivityComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<ActivityTableItem>;
  dataSource: ActivityTableDataSource;
  // loading: boolean;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  // displayedColumns = ['id', 'activityName', 'activityTime', 'ActivityDate', 'activityLocation', 'periodRegistration', 'activityDescription', 'hostTeacher','Detail', 'Enroll'];
  // displayedColumns = ['id', 'activityName', 'ActivityDate', 'activityLocation', 'periodRegistration', 'activityDescription', 'hostTeacher','Detail', 'Enroll'];
  displayedColumns = [
    "activityId",
    "activityName",
    "ActivityDate",
    "activityLocation",
    "periodRegistration",
    "activityDescription",
    "hostTeacher",
    "Detail"
  ];

  // activities: Activity[];
  // filter: string;
  // filter$: BehaviorSubject<any>;
  // Date: any;
  // MockData: Activity[];
  // constructor(private activityService: ActivityService,private router: Router, private route: ActivatedRoute) { }
  constructor(private activityService: ActivityService, private route: Router, public dialog: MatDialog,private authService:AuthenticationServiceService) {}

  hasRole(role: string) {
    return this.authService.hasRole(role);
  }

  // ngOnInit() {
  //   this.loading = true;
  //   this.activityService.getActivities()
  //     .subscribe(activities => {
  //       this.dataSource = new ActivityTableDataSource();
  //       this.dataSource.data = activities;
  //       this.dataSource.sort = this.sort;
  //       this.dataSource.paginator = this.paginator;
  //       this.table.dataSource = this.dataSource;
  //       this.activities=activities;
  //       this.filter$ = new BehaviorSubject<string>('');
  //       this.dataSource.filter$ = this.filter$;

  //       this.MockData = activities;
  //       this.convertDate();
  //       setTimeout(() => {
  //         this.loading = false;
  //       },500);
  //     }
  //     );
  // }
  ngOnInit() {
    this.activityService.getActivities().subscribe(activities => {
      this.dataSource = new ActivityTableDataSource(this.paginator, this.sort);
      this.dataSource.data = activities;
      console.log(activities);
    });
  }


  // convertDate(){
  //   for (var i=0; i <= this.MockData.length-1 ;i++){

  //       this.MockData[i].activityDate = new Date(2019,11,i,Math.floor(Math.random() * 24),0);

  //   }

  //       this.dataSource.data = this.MockData;
  //       // this.dataSource.sort = this.sort;
  //       // this.dataSource.paginator = this.paginator;
  //       this.table.dataSource = this.dataSource;
  //       this.filter$ = new BehaviorSubject<string>('');
  //       //this.dataSource.filter$ = this.filter$;
  // }

  // filterForm = new FormGroup({
  //   fromDate: new FormControl(),
  //   toDate: new FormControl()
  // });
  // get fromDate() { return this.filterForm.get('fromDate'); }
  // get toDate() { return this.filterForm.get('toDate'); }

  // filterDate(){
  //   console.log(this.Date.get('fromDate').value);
  //   console.log(this.Date.get('toDate').value);
  //   this.filter$.next([this.Date.value.fromDate,this.Date.value.toDate]);
  // }
  // ngAfterViewInit() {

  // }
  // applyFilter(filterValue: string) {
  //   this.filter$.next(filterValue.trim().toLowerCase());
  // }
  // applyDateFilter(value) {
  //     console.log(value.fromDate,value.toDate);
  //     this.filter$.next([value.fromDate,value.toDate]);

  // }
  // Enroll(act : Activity){
  //   console.log(act);
  //   if(confirm("Are you sure to enroll "+act.activityName)) {
  //     this.activityService.enrollActivity(act)
  //     .subscribe(() => {
  //     // this.router.navigate(['./admin/registered'])
  //   }, (error)=> {
  //     alert('could not save the value' + error)
  //   }
  //   )
  //   }
  // }
}
