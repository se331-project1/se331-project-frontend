import { Routes, RouterModule } from '@angular/router';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsAddComponent } from './add/students.add.component';
import { NgModule } from '@angular/core';
import { StudentTableComponent } from './student-table/student-table.component';
import { LecturerGuard } from '../Guard/lecturer-guard';
import { StudentAndLecturerGuard } from '../Guard/lecturer-and-student-guard';
import { ActivityRegistrationComponent } from '../admin/activity-registration/activity-registration.component';
import { StudentsRegisterComponent } from './students-register/students-register.component';
import { StudentActivityComponent } from './student-activity/student-activity.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { MyListComponent } from './my-list/my-list.component';



const StudentRoutes: Routes = [
    { path: 'add', component: StudentsAddComponent, canActivate: [LecturerGuard] },
    { path: 'list', component: StudentTableComponent, canActivate: [StudentAndLecturerGuard] },
    { path: 'detail/:id', component: StudentsViewComponent, canActivate: [StudentAndLecturerGuard] },
    { path: 'table', component: StudentTableComponent, canActivate: [StudentAndLecturerGuard] },
    { path: 'activities', component: ActivityRegistrationComponent },
    { path: 'student-register', component: StudentsRegisterComponent },
    { path: 'activitylist', component: StudentActivityComponent },
    { path: 'updateProfile', component: UpdateProfileComponent },
    { path: 'myList', component: MyListComponent }
];
@NgModule({
    imports: [RouterModule.forRoot(StudentRoutes)],
    exports: [RouterModule]
})
export class StudentRoutingModule { }
