import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import { HttpEvent, HttpEventType } from "@angular/common/http";
import { FileUploadService } from "src/app/service/file-upload.service";
import { environment } from "src/environments/environment";
import { BreakpointObserver } from "@angular/cdk/layout";
import { AuthenticationServiceService } from "src/app/authentication/authentication-service.service";
import { UserService } from "src/app/service/user-service";
import User from "src/app/entity/user";
import { HttpClient } from "@angular/common/http";
import { CoreEnvironment } from "@angular/compiler/src/compiler_facade_interface";
import { UserProfileDialogueComponent } from "src/app/my-nav/user-profile-dialogue/user-profile-dialogue.component";

@Component({
  selector: "app-update-profile",
  templateUrl: "./update-profile.component.html",
  styleUrls: ["./update-profile.component.css"]
})
export class UpdateProfileComponent implements OnInit {
  get diagnostic() {
    return JSON.stringify(this.form.value);
  }
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    breakpointObserver: BreakpointObserver,
    private authService: AuthenticationServiceService,
    private http: HttpClient,
    private fileUploadService: FileUploadService
  ) {}
  get user() {
    return this.authService.getCurrentUser();
  }
  uploadEndPoint: string;
  uploadedUrl: string;
  cUser = this.authService.getCurrentUser();
  image = this.cUser.image;
  pimage: string;
  progress: number;
  users: User[];
  form = this.fb.group({
    id: [{ value: this.cUser.id, disabled: true }],
    username: [{ value: this.cUser.username, disabled: true }],
    password: [null, Validators.required],
    email: [{ value: this.cUser.email, disabled: true }],
    image: [null, Validators.required]
  });

  validation_messages = {
    image: [{ type: "required", message: "please uplode your image" }],
    password: [{ type: "required", message: "the password is required" }]
  };

  ngOnInit(): void {
    this.uploadEndPoint = environment.updateProfileApi;
  }

  submit() {
    const fmVal = this.form.value;
    const newUsr: User = {
      id: this.cUser.id,
      username: this.cUser.username,
      password: fmVal.password,
      firstname: this.cUser.firstname,
      lastname: this.cUser.lastname,
      email: this.cUser.email,
      enabled: this.cUser.enabled,
      image: fmVal.image,
      lastPasswordResetDate: this.cUser.lastPasswordResetDate,
      authorities: this.cUser.authorities,
      appUser: this.cUser.appUser
    };

    this.http.put<any>(this.uploadEndPoint + "/" + this.cUser.username, newUsr).subscribe(data => {
      this.userService.load();
      alert("Update successfully");
      alert("You have to login again!");
    });

    this.authService.logout();
    this.authService.login(newUsr.username, newUsr.password);

    this.router.navigate(["/list"]);
  }
}
