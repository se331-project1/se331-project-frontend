import { Component, OnInit } from "@angular/core";
import { StudentService } from "src/app/service/student-service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import Student from "src/app/entity/student";
import { HttpEvent, HttpEventType, HttpClient } from "@angular/common/http";
import { FileUploadService } from "src/app/service/file-upload.service";
import { environment } from "src/environments/environment";
import Lecturer from 'src/app/entity/lecturer';
import User from 'src/app/entity/user';
import { UserService } from 'src/app/service/user-service';
import { AuthenticationServiceService } from 'src/app/authentication/authentication-service.service';
import StuRegister from 'src/app/entity/stuRegister';

@Component({
  selector: "app-students-register",
  templateUrl: "./students-register.component.html",
  styleUrls: ["./students-register.component.css"]
})
export class StudentsRegisterComponent implements OnInit {
  get diagnostic() {
    return JSON.stringify(this.form.value);
  }

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private authService: AuthenticationServiceService
  ) { }
  uploadEndPoint: string;
  uploadedUrl: string;
  progress: number;
  students: Student[];
  cUser = this.authService.getCurrentUser();
  form = this.fb.group({
    id: [""],
    name: [null, Validators.required],
    surname: [null, Validators.required],
    image: [null, Validators.required],
    dob: [null, Validators.required],
    studentId: [null, Validators.compose([Validators.required, Validators.maxLength(13)])],
    username: [
      null,
      Validators.compose([Validators.required, Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")])
    ],
    password: [null, Validators.required]
  });

  validation_messages = {
    name: [{ type: "required", message: "the name is required" }],
    surname: [{ type: "required", message: "the surname is required" }],
    image: [{ type: "required", message: "please uplode your image" }],
    dob: [{ type: "required", message: "please fill your date of birth" }],
    studentId: [
      { type: "required", message: "student id is required" },
      { type: "maxlength", message: "student id is too long" }
    ],
    username: [
      { type: "required", message: "the email is required" },
      { type: "pattern", message: "please enter correct email" }
    ],
    password: [{ type: "required", message: "the password is required" }]
  };

  ngOnInit(): void {
    this.uploadEndPoint = environment.studentRegisterApi;
  }

  submit() {
    let fmVal = this.form.value;
    let newS: StuRegister = {
      id: 0,
      firstname: fmVal.name,
      lastname: fmVal.surname,
      image: fmVal.image,
      dob: fmVal.dob,
      studentId: fmVal.studentId,
      username: fmVal.name,
      password: fmVal.password,
      email: fmVal.username,
      lastPasswordResetDate: null,
      authorities: null,
      gpa: 0
    }

    this.http.post<any>(environment.studentRegisterApi, newS)
      .subscribe(data => {
        alert('Update successfully');
        alert('You have to login again!');
      });

    this.router.navigate(['/list'])
  }

  onSelectedFilesChanged(files?: FileList) { }

}
