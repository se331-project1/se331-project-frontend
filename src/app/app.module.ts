import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatExpansionModule,
  MatBadgeModule,
  MatAutocompleteModule
} from "@angular/material";
import { MatDialogModule } from "@angular/material/dialog";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { FileNotFoundComponent } from "./shared/file-not-found/file-not-found.component";
import { HttpClientModule, HTTP_INTERCEPTORS, HttpHeaders } from "@angular/common/http";
import { LayoutModule } from "@angular/cdk/layout";
import { MatFileUploadModule } from "mat-file-upload";
import { MatInputModule } from "@angular/material";
import { MyNavComponent } from "./my-nav/my-nav.component";
import { NgModule } from "@angular/core";
import { StudentRestImplService } from "./service/student-rest-impl.service";
import { StudentRoutingModule } from "./students/student-routing.module";
import { StudentService } from "./service/student-service";
import { StudentTableComponent } from "./students/student-table/student-table.component";
import { StudentsAddComponent } from "./students/add/students.add.component";
import { StudentsComponent } from "./students/list/students.component";
import { StudentsViewComponent } from "./students/view/students.view.component";
import { LoginComponent } from "./login/login.component";
import { JwtIterceptorServiceService } from "./helpers/jwt-iterceptor-service.service";
import { ActivityRegistrationComponent } from "./admin/activity-registration/activity-registration.component";
import { ActivityService } from "./service/activity-service";
import { ActivityRestImplService } from "./service/activity-rest-impl.service";
import { StudentsRegisterComponent } from "./students/students-register/students-register.component";
import { UserProfileDialogueComponent } from "./my-nav/user-profile-dialogue/user-profile-dialogue.component";
import { StudentActivityComponent } from "./students/student-activity/student-activity.component";
import { LecturerServiceService } from "./service/lecturer-service.service";
import { LecturerRestImplService } from "./service/lecturer-rest-impl.service";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { UserService } from "./service/user-service";
import { UserRestImplService } from "./service/user-rest-impl.service";
import { UpdateProfileComponent } from "./students/update-profile/update-profile.component";
import { MyListComponent } from './students/my-list/my-list.component';

@NgModule({
  entryComponents: [MyNavComponent, UserProfileDialogueComponent],
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    StudentTableComponent,
    ActivityRegistrationComponent,
    StudentsRegisterComponent,
    LoginComponent,
    StudentActivityComponent,
    UserProfileDialogueComponent,
    UpdateProfileComponent,
    StudentsRegisterComponent,
    UserProfileDialogueComponent,
    MyListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatProgressBarModule,
    StudentRoutingModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatFileUploadModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatBadgeModule,
    MatDialogModule,
    MatAutocompleteModule,
    ScrollingModule
  ],
  providers: [
    { provide: StudentService, useClass: StudentRestImplService },
    { provide: HTTP_INTERCEPTORS, useClass: JwtIterceptorServiceService, multi: true },
    { provide: ActivityService, useClass: ActivityRestImplService },
    { provide: LecturerServiceService, useClass: LecturerRestImplService },
    { provide: UserService, useClass: UserRestImplService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
