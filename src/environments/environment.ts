// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  activityApi: "http://localhost:8080/activities",
  studentApi: "http://localhost:8080/students",
  uploadApi: "http://localhost:8080/uploadFile",
  commentApi: "http://localhost:8080/comments",
  authenticationApi: "http://localhost:8080/auth",
  lecturerApi: "http://localhost:8080/lecturers",
  updateProfileApi: "http://localhost:8080/users",
  studentRegisterApi: "http://localhost:8080/sRegister"
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
